'use strict';

const allRules = {
    'no-fontSize-literals': require('./rules/no-fontSize-literals'),
    'no-spacing-literals': require('./rules/no-spacing-literals'),
    'no-color-literals': require('./rules/no-color-literals'),
    'no-radius-literals': require('./rules/no-radius-literals'),
  };

function configureAsError(rules) {
    const result = {};
    for (const key in rules) {
      if (!rules.hasOwnProperty(key)) {
        continue;
      }
      result['@momo-platform/momo/' + key] = 2;
    }
    return result;
}
module.exports = {
    deprecatedRules: {},
    rules: allRules,
    rulesConfig: {
      'no-fontSize-literals': 2,
      'no-spacing-literals': 2,
      'no-color-literals': 2,
      'no-radius-literals': 2
    },
    configs: {
      all: {
        plugins: [
          '@momo-platform/momo'
        ],
        parserOptions: {
          ecmaFeatures: {
            jsx: true
          }
        },
        rules: configureAsError(allRules)
      }
    }
};
