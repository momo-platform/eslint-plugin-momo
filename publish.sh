#!/bin/bash
rm -rf dist
mkdir dist

cp ./package.json ./dist
cp ./index.js ./dist/index.js
cp -r ./rules/ ./dist/rules
cp -r ./util/ ./dist/util

#babel component to dist
babel ./dist -d dist --copy-files

#copy option
#cp -r ./src/ dist


#npm login
#publish dist to npm
cd dist
npm publish --access=public

# GET VERSION from package.json
VERSIONSTRING=( v$(jq .version package.json) )
VERSION=(${VERSIONSTRING//[\"]/})
echo VERSION: $VERSION

curl -X POST -H 'Content-Type: application/json' 'https://chat.googleapis.com/v1/spaces/AAAAbP8987c/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=UGSFRvk_oYb9uGsAgs31bVvMm6jDkmD8zihGm3eyaQA%3D&threadKey=JoaXTEYaNNkl' -d '{"text": "MoMo Eslint Plugin new version release: '*"$VERSION"*' `https://www.npmjs.com/package/@momo-platform/eslint-plugin-momo`"}'
# #clear dist and package json

cd ..
rm -rf dist
